package top.hmtools.wxmp.user.apis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.BaseTest;
import top.hmtools.wxmp.user.model.BatchTagParam;
import top.hmtools.wxmp.user.model.TagFunsParam;
import top.hmtools.wxmp.user.model.TagFunsResult;
import top.hmtools.wxmp.user.model.TagListParam;
import top.hmtools.wxmp.user.model.TagListResult;
import top.hmtools.wxmp.user.model.TagParam;
import top.hmtools.wxmp.user.model.TagWapperParam;
import top.hmtools.wxmp.user.model.TagWapperResult;
import top.hmtools.wxmp.user.model.TagsWapperResult;

/**
 * 用户管理--用户标签管理  测试
 * @author HyboWork
 *
 */
public class ITagsApiTest extends BaseTest {
	
	private ITagsApi tagsApi ;
	

	/**
	 * 1、创建标签
	 */
	@Test
	public void testCreate() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setName("人才");
		param.setTag(tag);
		TagWapperResult create = this.tagsApi.create(param);
		this.printFormatedJson("用户标签管理--1、创建标签", create);
	}

	/**
	 * 2. 获取公众号已创建的标签
	 */
	@Test
	public void testGet() {
		TagsWapperResult tagsWapperResult = this.tagsApi.get();
		this.printFormatedJson("用户标签管理--2. 获取公众号已创建的标签", tagsWapperResult);
	}

	@Test
	public void testUpdate() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setId(100);
		tag.setName("super人才");
		param.setTag(tag);
		ErrcodeBean update = this.tagsApi.update(param);
		this.printFormatedJson("用户标签管理--3. 编辑标签", update);
	}

	@Test
	public void testDelete() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setId(100);
		param.setTag(tag);
		ErrcodeBean delete = this.tagsApi.delete(param);
		this.printFormatedJson("用户标签管理--4. 删除标签", delete);
	}

	@Test
	public void testGetFunsOfTag() {
		TagFunsParam param = new TagFunsParam();
		param.setTagid(101);
		TagFunsResult funsOfTag = this.tagsApi.getFunsOfTag(param);
		this.printFormatedJson("用户标签管理--5. 获取标签下粉丝列表", funsOfTag);
	}

	@Test
	public void testBatchTagging() {
		BatchTagParam batchTagParam = new BatchTagParam();
		batchTagParam.setTagid(101);
		List<String> aa = new ArrayList<String>();
		aa.add("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		batchTagParam.setOpenid_list(aa);
		ErrcodeBean batchTagging = this.tagsApi.batchTagging(batchTagParam);
		this.printFormatedJson("用户标签管理--1. 批量为用户打标签", batchTagging);
	}

	@Test
	public void testBatchUntagging() {
		BatchTagParam batchTagParam = new BatchTagParam();
		batchTagParam.setTagid(101);
		List<String> aa = new ArrayList<String>();
		aa.add("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		batchTagParam.setOpenid_list(aa);
		ErrcodeBean batchUntagging = this.tagsApi.batchUntagging(batchTagParam);
		this.printFormatedJson("用户标签管理--2. 批量为用户取消标签", batchUntagging);
	}

	@Test
	public void testGetTaglistByOpenid() {
		TagListParam param = new TagListParam();
		param.setOpenid("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		TagListResult taglistByOpenid = this.tagsApi.getTaglistByOpenid(param);
		this.printFormatedJson("用户标签管理--3. 获取用户身上的标签列表", taglistByOpenid);
	}

	@Override
	public void initSub() {
		this.tagsApi = this.wxmpSession.getMapper(ITagsApi.class);
	}

}
