package top.hmtools.wxmp.user.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.user.model.BatchUserInfoParam;
import top.hmtools.wxmp.user.model.BatchUserInfoResult;
import top.hmtools.wxmp.user.model.UserInfoParam;
import top.hmtools.wxmp.user.model.UserInfoResult;

@WxmpMapper
public interface IUnionIDApi {

	/**
	 * 获取用户基本信息（包括UnionID机制）
	 * @param userInfoParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/user/info")
	public UserInfoResult getUserInfo(UserInfoParam userInfoParam);
	
	/**
	 * 批量获取用户基本信息
	 * @param batchUserInfoParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/user/info/batchget")
	public BatchUserInfoResult getBatchUserInfo(BatchUserInfoParam batchUserInfoParam);
}
