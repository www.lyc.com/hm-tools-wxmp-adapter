package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使用此注解修饰后，该类才允许用于接收微信事件通知
 * <br>仿照spring MVC实现，使用时，还需结合 {@link top.hmtools.wxmp.core.annotation.WxmpRequestMapping}
 * @author HyboWork
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpController {

}
