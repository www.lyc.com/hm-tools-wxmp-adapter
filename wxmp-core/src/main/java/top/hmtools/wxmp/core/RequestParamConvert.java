package top.hmtools.wxmp.core;

import java.util.HashMap;

import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * 请求参数转换
 * @author HyboWork
 *
 */
public interface RequestParamConvert {

	/**
	 * 自行组装请求参数
	 * @param params
	 * @return
	 */
	public MultipartEntityBuilder convert(HashMap<String, Object> params);
}
