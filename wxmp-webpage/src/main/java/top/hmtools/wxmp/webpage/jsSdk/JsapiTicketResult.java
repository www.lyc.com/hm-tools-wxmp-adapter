package top.hmtools.wxmp.webpage.jsSdk;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * 微信公众号 js sdk中获取jsapi_ticket信息的实体类
 * @author Hybomyth
 *
 */
public class JsapiTicketResult extends ErrcodeBean {

	/**
	 * 用于生成JS-SDK权限验证的签名用的jsapi_ticket
	 */
	private String ticket;
	
	/**
	 * 有效期为7200秒，通过access_token来获取。由于获取jsapi_ticket的api调用次数非常有限，频繁刷新jsapi_ticket会导致api调用受限，影响自身业务，开发者必须在自己的服务全局缓存jsapi_ticket
	 */
	private Long expires_in;
	
	/**
	 * 上一次刷新ticket值的时间戳
	 */
	private Long lastRefreshTimastamp;
	
	/**
	 * 是否已经过期
	 * @return
	 */
	public boolean isExpiresOut(){
		if(this.lastRefreshTimastamp==null){
			return true;
		}
		
		long cha = System.currentTimeMillis()-this.lastRefreshTimastamp;
		return cha>=7200*1000;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
		this.lastRefreshTimastamp = System.currentTimeMillis();
	}

	public Long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}

	/**
	 * 当ticket被赋值时，时间戳值会自动刷新
	 * @return
	 */
	public Long getLastRefreshTimastamp() {
		return lastRefreshTimastamp;
	}

	@Override
	public String toString() {
		return "JsapiTicketResult [ticket=" + ticket + ", expires_in=" + expires_in + ", lastRefreshTimastamp="
				+ lastRefreshTimastamp + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
