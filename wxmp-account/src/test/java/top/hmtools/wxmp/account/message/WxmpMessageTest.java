package top.hmtools.wxmp.account.message;

import org.junit.Test;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;

/**
 * 模拟测试接收微信公众号自定义菜单相关事件
 * @author HyboWork
 *
 */
public class WxmpMessageTest {

	@Test
	public void executeMessageTest() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		VerifyMessageTestController menuMessageTestController = new VerifyMessageTestController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);
		
		//1 资质认证成功（此时立即获得接口权限）
		String xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442401156</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[qualification_verify_success]]></Event>    <ExpiredTime>1442401156</ExpiredTime> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	
		
		//2 资质认证失败
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442401156</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[qualification_verify_fail]]></Event>    <FailTime>1442401122</FailTime>    <FailReason><![CDATA[by time]]></FailReason> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		//3 名称认证成功（即命名成功）
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442401093</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[naming_verify_success]]></Event>    <ExpiredTime>1442401093</ExpiredTime> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		//4 名称认证失败（这时虽然客户端不打勾，但仍有接口权限
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442401061</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[naming_verify_fail]]></Event>    <FailTime>1442401061</FailTime>    <FailReason><![CDATA[by time]]></FailReason> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		//5 年审通知
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442401004</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[annual_renew]]></Event>    <ExpiredTime>1442401004</ExpiredTime> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		//6 认证过期失效通知
		xml = "<xml>  <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>1442400900</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[verify_expired]]></Event>    <ExpiredTime>1442400900</ExpiredTime> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
