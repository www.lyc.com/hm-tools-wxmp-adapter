# 配置
- 所有的配置项均已加入 additional-spring-configuration-metadata.json 文件中，使用 eclipse 开发时，在安装了spring IDE插件后，在配置文件中进行配置时，按下“alt+/”组合键时，会有自动补全提示。

## 配置项说明
- `hm_tools.js_css.enabled=true`	是否启用本jar包组件功能，当配置为“true”时或者不配置该值时均表示启用，为“false”时则不启用。
- `hm_tools.js_css.js_uri=/get_js`		配置获取javascript文件内容的请求uri，缺省值为：“/js”。
- `hm_tools.js_css.css_uri=/get_css`	配置获取CSS文件内容的请求uri，缺省值为：“/css”。
- `hm_tools.js_css.refresh_js_uri=/refresh_js`	配置刷新javascript文件缓存内容的请求uri，缺省值为：“/refresh/js”。
- `hm_tools.js_css.refresh_css_uri=/refresh_css`	配置刷新css文件缓存内容的请求uri，缺省值为：“/refresh/css”。
- `hm_tools.js_css.js_files_paths=static`		配置获取javascript文件内容的磁盘路径集合（均相对于工程项目的classpath），以英文逗号（,）分隔，缺省值为：“static,resources”。
- `hm_tools.js_css.css_files_paths=static`		配置获取css文件内容的磁盘路径集合（均相对于工程项目的classpath），以英文逗号（,）分隔，缺省值为：“static,resources”。
- `hm_tools.js_css.encoding=UTF-8`		获取的文件内容的字符编码格式名称，缺省为“UTF-8”。
- `hm_tools.js_css.list_js_uri=/list_js`		获取所有javascript文件列表请求uri，缺省值为：“/list/js”。
- `hm_tools.js_css.list_css_uri=/list_css`		获取所有css文件列表请求uri，缺省值为：“/list/css”。
- `hm_tools.js_css.src_uri=src`	通用的获取静态资源文件内容uri，仅获取单个文件，且该文件必须在配置的js_files_paths、css_files_paths路径下，用于解决css文件中含有相对路径引用其它文件的问题。缺省值为：“/src”。
- `hm_tools.js_css.yui.js_uri= /yui/get_js`  配置获取使用yui压缩后javascript文件内容的请求uri，缺省值为：“/yui/js”。
- `hm_tools.js_css.yui.css_uri= /yui/get_css`   配置获取使用yui压缩后CSS文件内容的请求uri，缺省值为：“/yui/css”。

## 完整的yaml配置示例
```
hm_tools:
  js_css:
    enabled: true
    js_uri: /js
    css_uri: /css
    refresh_js_uri: /refresh/js
    refresh_css_uri: /refresh/css
    js_files_paths: 'static,resources'
    css_files_paths: 'static,resources'
    encoding: UTF-8
    list_js_uri: /list/js
    list_css_uri: /list/css
    src_uri: /src
    yui:
      js_uri: /yui/js
      css_uri: /yui/css
      
```

## 完整的properties配置示例
```
hm_tools.js_css.enabled=true
hm_tools.js_css.js_uri=
hm_tools.js_css.css_uri= 
hm_tools.js_css.list_js_uri= 
hm_tools.js_css.list_css_uri=  /haha/list/css

hm_tools.js_css.yui.js_uri=
hm_tools.js_css.yui.css_uri=

hm_tools.js_css.refresh_js_uri= 
hm_tools.js_css.refresh_css_uri=  
hm_tools.js_css.js_files_paths=static,resources
hm_tools.js_css.css_files_paths=static,resources
hm_tools.js_css.encoding=UTF-8

hm_tools.js_css.src_uri=
```

<icp/>