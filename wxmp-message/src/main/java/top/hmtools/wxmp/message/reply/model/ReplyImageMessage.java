package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 回复图片消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>12345678</CreateTime>
	<MsgType><![CDATA[image]]></MsgType>
	<Image>
		<MediaId><![CDATA[media_id]]></MediaId>
	</Image>
</xml>
 * }
 * @author hybo
 *
 */
public class ReplyImageMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3448579939645443519L;
	
	/**
	 * 图片信息
	 */
	@XStreamAlias("Image")
	private ReplyImage image;

	public ReplyImage getImage() {
		return image;
	}

	public void setImage(ReplyImage image) {
		this.image = image;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "ReplyImageMessage [image=" + image + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}


	
}
