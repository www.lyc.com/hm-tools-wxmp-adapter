package top.hmtools.wxmp.message.template.model.event;

import org.junit.Test;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;

public class TemplateMessageSendedEventTest {

	@Test
	public void test() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		TemplateMessageSendedEventController menuMessageTestController = new TemplateMessageSendedEventController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);

		//
		String xml = "<xml>   <ToUserName><![CDATA[gh_7f083739789a]]></ToUserName>    <FromUserName><![CDATA[oia2TjuEGTNoeX76QEjQNrcURxG8]]></FromUserName>    <CreateTime>1395658920</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[TEMPLATESENDJOBFINISH]]></Event>    <MsgID>200163836</MsgID>    <Status><![CDATA[success]]></Status> </xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	}

}
